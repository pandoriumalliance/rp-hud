#PANDORIUM ALLIANCE | RP HUD | FULL#
###Powered by Co Starsmith on an Elliptical###
####Version 0.80 # Released 04/04/2017####

══════════════════════════════════════════════════════

◣◥ | First-timer Information | ◤◢
     
As a member of Pandorium Alliance you will be able to earn your own resources 
and money.
                
This HUD (Heads Up Display) allows you to interact with your fellow comrades 
and your surroundings. This includes and is not limited to buying your own
Essence(ess), using ess to purchase weapons, gear, gadgets, and other 
equipment. In future releases you will have the ability to gain experience 
and level up by interacting with items in registered sims.
         
As a RP HUD user you are obligated to make suggestions and report any issues 
you are having.
     
══════════════════════════════════════════════════════

◣◥ | Instructions & Guidance | ◤◢
     
➊ Grid Communications

  * Operates on channel 77. 
    - Example type "/77 hello grid" (without quotes)
    - See who is only by typing "/5ping" (without quotes)
        
➋ Experience and Levels

  * How to gain experience (xp) to level up:
    - You can gain xp and level up by increasing your energy level, see section 4 "Energy" for how to do this.
    - Future installments will include sim objectives 
      (e.g. capture a point, lock a door, use equipment around the sim)

  * Why are there levels (lvl)?
    - Levels are used to access more powerful weaponry and equipment 
         
➌ Resources

  * Essence (ess):
    - This is our in-house currency. Pandorium Alliance has a micro-economic system allowing our members and authorized users to buy and sell weapons and equipment.  
    - You can buy Official Pandorium Alliance Equipment. This equipment is versioned (infinite updates) and managed by our R&D team.
    - You can sell unofficial equipment using our Unversioned vendors. Contact Co Starsmith (co.starsmith) for more information. You can set your own essence price and be funded the amount on any given purchase.
    - You can earn essence by participating in group activites such as raids and defenses. It is important for you as an RP user to tell your commanding officer that you participated in such and such event so you can get credit for it.
    - In the future you will also be able to mine essence in designated areas around our sim.

  * Wood: (will be planned and implemented later on)

  * Rock: (will be planned and implemented later on)

  * Metal: (will be planned and implemented later on)
   
➍ Energy
 
  * Sleep
    - One of the most effective ways to replenish your energy is sleeping.
    - Type ":rez sleepingbag" (without quotes) to drop a sleeping bag and sit on it to start 'sleeping'. 
    - You will gain experience through this method.

  * Food and Drink (will be planned and implemented later on)
    - This method will replenish your energy but you will not receive as much experience as to sleeping. 

  * Emergency Energy (will be planned and implemented later on)
    - This will fill your energy completely in emergency situations (e.g. in combat)
        
➎ Teleportation System (will be planned and implemented later on)

➏ Combat Integration
  
  * Currently using the RED HUD for combat, these two huds will be combined in the  future.

➐ Admin Controls (local chat or channel 2)

  * Essence can be given out for raids and defenses, and or bonuses.
  * Give an individual Essence    
    - Type ":ess give indv <amount> <username>" (without quotes or brackets'<>')
    - usernames for avatars with a first and last name look like "first.last"
  * Give a group essence all at once
    - Type ":ess give group <amount>" (without quotes or brackets '<>')
    - this is within the 'say' chat range (20 meters)

➑ 
➒ 
➓ 
     
   
══════════════════════════════════════════════════════

◣◥ | The Change Log | ◤◢

    Release 0.80  - 04/03/2017

            ✔ Integrated with Group Energy Doors.

    Release 0.78  - 03/26/2017
            
            ✔ Click on any item display will refresh the numbers
            ✔ Sleeping bag spamming messages
            ✔ Minor Code Improvements
            
                                              
    Release 0.75  - 03/20/2017
    
            ✔ Completely Redesigned and Re-engineered
            ✔ Implementation of Resources
            ✔ Essence Transactions
            ✔ Sleeping Bag Rezzing position fixed